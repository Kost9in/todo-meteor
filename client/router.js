
Router.configure({
    notFoundTemplate: 'notFound'
});

Router.route('type', {
    path: '/:type',
    template: 'main'
});