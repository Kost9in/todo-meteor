
import { Todo } from '../../../lib/collections/todo.js';

Meteor.subscribe('todos');

const filters = new ReactiveVar([
    { key: 'all', title: 'All', cls: '' },
    { key: 'active', title: 'Active', cls: '' },
    { key: 'closed', title: 'Closed', cls: '' }
]);

const getDate = (date) => {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    return `${(day > 9) ? day : `0${day}`}/${(month > 9) ? month : '0' + month}/${year}
            ${(hours > 9) ? hours : `0${hours}`}:${(minutes > 9) ? minutes : `0${minutes}`}:${(seconds > 9) ? seconds : `0${seconds}`}`;
};

Template.todoList.helpers({
    filtersList() {
        return filters.get().map(item => (Router.current().params.type === item.key) ? Object.assign({}, item, { cls: 'active' }) : item);
    },
    activeFilter() {
        return Router.current().params.type;
    },
    todos() {
        const filter = {};
        let sort = {closed: 1};
        const type = Router.current().params.type;
        if (type === 'active') {
            filter.closed = false;
        } else if (type === 'closed') {
            filter.closed = true;
        }
        switch (Session.get('sort')) {
            case 'name-asc':
                sort.text = 1;
                break;
            case 'name-desc':
                sort.text = -1;
                break;
            case 'date-asc':
                sort.createdAt = 1;
                break;
            case 'date-desc':
                sort.createdAt = -1;
                break;
        }
        return Todo.find(filter, { sort: sort }).map(todo => {
            todo.class = (todo.closed) ? 'closed' : '';
            todo.createdAt = getDate(todo.createdAt);
            return todo;
        });
    }
});

Template.todoList.events({
    'click .todo-title'(event) {
        event.preventDefault();
        Todo.update(event.currentTarget.getAttribute('data-id'), {
            $set: { closed: !this.closed }
        });
    },
    'click .todo-edit'(event) {
        event.preventDefault();
        const id = event.currentTarget.getAttribute('data-id');
        Session.set('edit', id);
        const editingTodo = Todo.findOne(id);
        const value = (editingTodo && editingTodo.text) ? editingTodo.text : '';
        const input = document.querySelector('.editForm input[name="todo"]');
        if (input) {
            input.value = value;
        }
    },
    'click .todo-delete'(event) {
        event.preventDefault();
        Session.set('delete', event.currentTarget.getAttribute('data-id'));
    }
});