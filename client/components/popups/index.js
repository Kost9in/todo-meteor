
import { Todo } from '../../../lib/collections/todo.js';

const deletingText = new ReactiveVar('');

Template.popups.onCreated(() => {
    Session.set('add', false);
    Session.set('edit', false);
    Session.set('delete', false);
});

Template.popups.helpers({
    opened() {
        return Session.get('add') || Session.get('edit') || Session.get('delete');
    },
    isAdd() {
        return Session.get('add');
    },
    isEdit() {
        return Session.get('edit');
    },
    isDelete() {
        return Session.get('delete');
    },
    deletingTodo() {
        const deletingTodo = Todo.findOne(Session.get('delete'));
        if (deletingTodo && deletingTodo.text) {
            deletingText.set(deletingTodo.text);
        }
        return deletingText.get();
    }
});

const closePopups = () => {
    Session.set('add', false);
    Session.set('edit', false);
    Session.set('delete', false);
};

Template.popups.events({
    'click .fade, click .popup .cancel-button'(event) {
        event.preventDefault();
        closePopups();
    },
    'focus .addForm input[name="todo"], focus .editForm input[name="todo"]'(event) {
        event.preventDefault();
        event.currentTarget.classList.remove('error');
    },
    'submit .addForm, submit .editForm'(event) {
        event.preventDefault();
        const form = event.currentTarget;
        const input = form.querySelector('input[name="todo"]');
        const todo = input.value.trim();
        if (todo) {
            const type = form.getAttribute('data-form');
            if (type === 'add') {
                Todo.insert({
                    text: todo,
                    createdAt: new Date(),
                    closed: false
                });
            }
            if (type === 'edit') {
                Todo.update(Session.get('edit'), {
                    $set: { text: todo }
                });
            }
            closePopups();
        } else {
            input.classList.add('error');
        }
    },
    'click .delete-button'(event) {
        event.preventDefault();
        Todo.remove(Session.get('delete'));
        Session.set('delete', false);
    }
});