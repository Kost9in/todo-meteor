
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

const openSort = new ReactiveVar(false);
const sortTypes = new ReactiveVar([
    { key: 'name-asc', title: 'Name', icon: 'fa-sort-alpha-asc', cls: '' },
    { key: 'name-desc', title: 'Name', icon: 'fa-sort-alpha-desc', cls: '' },
    { key: 'date-asc', title: 'Date', icon: 'fa-sort-numeric-asc', cls: '' },
    { key: 'date-desc', title: 'Date', icon: 'fa-sort-numeric-desc', cls: '' }
]);

Template.header.onCreated(() => {
    Session.setDefault('sort', 'date-desc');
});

Template.header.helpers({
    sortTypes() {
        return sortTypes.get().map(item => (Session.get('sort') === item.key) ? Object.assign({}, item, { cls: 'selected' }) : item);
    },
    activeSort(filed) {
        const sort = Session.get('sort');
        const found = sortTypes.get().find(item => item.key === sort);
        if (found) {
            return (found[filed]) ? found[filed] : found.type;
        } else {
            return false;
        }
    },
    openSort() {
        return openSort.get();
    }
});

Template.header.events({
    'click header .select .change-sort'(event) {
        event.preventDefault();
        openSort.set(!openSort.get());
    },
    'click header .select ul li a'(event) {
        event.preventDefault();
        Session.set('sort', event.currentTarget.getAttribute('data-sort'));
        openSort.set(false);
    },
    'click header .add a'(event) {
        event.preventDefault();
        Session.set('add', true)
    },
});