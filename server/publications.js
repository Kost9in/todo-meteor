
import { Todo } from '../lib/collections/todo.js';

Meteor.publish('todos', () => {
    return Todo.find();
});